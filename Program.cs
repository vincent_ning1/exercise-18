﻿using System;

namespace exercise_18
{
    class Program
    {
        static void Main(string[] args)
        {
           //Start the program with Clear();
           Console.Clear();
           int num1 = 5;
           int num2 = 10;
           int num3 =15;
           int num4 = 20;
           int num5 = 25;
           Console.WriteLine($"The value of num1 ={num1} ");
           Console.WriteLine($"The value of num2 = {num2}");
           Console.WriteLine($"The value of num3 = {num3}");
           Console.WriteLine($"The value of num4 = {num4} ");
           Console.WriteLine($"The value of num5 = {num5} ");
           Console.ReadKey();
           int num6=30;
    
           Console.WriteLine($"The value of num1 ={num1} ");
           Console.WriteLine($"The value of num2 = {num2}");
           Console.WriteLine($"The value of num3 = {num3}");
           Console.WriteLine($"The value of num4 = {num4} ");
           Console.WriteLine($"The value of num5 = {num5} ");
           Console.WriteLine($"The value of num6 = {num6} ");
           Console.WriteLine($"");
           Console.WriteLine($"");
           Console.WriteLine($"");
           Console.WriteLine($"");
           Console.WriteLine($"");
           Console.WriteLine($"");
           Console.WriteLine($"num1+num2={num1+num2}");
           Console.WriteLine($"num3+num4={num3+num4}");
           Console.WriteLine($"num5+num6={num5+num6}");
           Console.ReadKey();
           Console.WriteLine($"The value of num1 ={num1} ");
           Console.WriteLine($"The value of num2 = {num2}");
           Console.WriteLine($"The value of num3 = {num3}");
           Console.WriteLine($"The value of num4 = {num4} ");
           Console.WriteLine($"The value of num5 = {num5} ");
           Console.WriteLine($"The value of num6 = {num6} ");
           Console.WriteLine($"");
           Console.WriteLine($"");
           Console.WriteLine($"");
           Console.WriteLine($"");
           Console.WriteLine($"");
           Console.WriteLine($"num1+num6={num1=num1+num6} ");


           
           //End the program with blank line and instructions
           Console.ResetColor();
           Console.WriteLine();
           Console.WriteLine("Press <Enter> to quit the program");
           Console.ReadKey();
           var str = "salkfhkasfh";
           Console.Write($"{str*3}");
        }
    }
}
